﻿using System;
using AspCoreCRUD.Data;
using AspCoreCRUD.Model;
using AspCoreCRUD.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreCRUD.Controller
{
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Employee>> Get()
        {
            return await _employeeRepository.Get();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> Get(int id)
        {
            return await _employeeRepository.Get(id);
        }
    }
}

