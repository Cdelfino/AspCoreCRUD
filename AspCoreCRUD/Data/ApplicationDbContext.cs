﻿using System;
using AspCoreCRUD.Model;
using Microsoft.EntityFrameworkCore;

namespace AspCoreCRUD.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {

        }

        public DbSet<Employee> Employees { get; set; }
    }
}

