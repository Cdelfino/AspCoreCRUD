﻿using System;
using AspCoreCRUD.Data;
using AspCoreCRUD.Model;
using Microsoft.EntityFrameworkCore;

namespace AspCoreCRUD.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public readonly ApplicationDbContext _context;

        public EmployeeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Employee> Create(Employee employee)
        {
            _context.Employees.Add(employee);
            await _context.SaveChangesAsync();
            return employee;
        }

        public async Task Delete(int id)
        {
            var employeeDelete = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employeeDelete);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Employee>> Get()
        {
            return await _context.Employees.ToListAsync();
        }

        public async Task<Employee> Get(int id)
        {
            return await _context.Employees.FindAsync(id);
        }

        public async Task Update(Employee employee)
        {
            _context.Entry(employee).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}

