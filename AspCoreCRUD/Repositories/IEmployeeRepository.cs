﻿using System;
using AspCoreCRUD.Model;

namespace AspCoreCRUD.Repositories
{
    public interface IEmployeeRepository
    {
        Task<IEnumerable<Employee>> Get();

        Task<Employee> Get(int Id);

        Task<Employee> Create(Employee employee);

        Task Update(Employee employee);

        Task Delete(int Id);
    }
}

